<!DOCTYPE html>
<html>
<head>
  <title>Performance Timeline Level 2</title>
  <meta charset='utf-8'>
  <script src='//www.w3.org/Tools/respec/respec-w3c-common' async class=
  'remove'>
  </script>
  <script class='remove'>
  var respecConfig = {
    shortName: "performance-timeline-2",
    specStatus: "ED",
    edDraftURI: "https://w3c.github.io/performance-timeline/",
    // publishDate: "2013-10-23",
    editors: [{
      name: "Ilya Grigorik",
      url: "https://www.igvita.com/",
      mailto: "igrigorik@gmail.com",
      company: "Google",
      companyURL: "https://www.google.com/",
      w3cid: "56102"
    },{
      name: "Jatinder Mann",
      mailto: "jmann@microsoft.com",
      company: "Microsoft Corp.",
      note: "Until November 2014"
    },{
      name: "Zhiheng Wang",
      company: "Google",
      note: "Until July 2013"
    }],
    wg: "Web Performance Working Group",
    wgURI: "http://www.w3.org/2010/webperf/",
    wgPublicList: "public-web-perf",
    subjectPrefix: "[Performance Timeline]",
    format: "markdown",
    otherLinks: [{
      key: 'Repository',
      data: [{
        value: 'We are on GitHub',
        href: 'https://github.com/w3c/performance-timeline/'
      }, {
        value: 'Commit history',
        href: 'https://github.com/w3c/performance-timeline/commits/gh-pages/index.html'
      }, {
        value: 'File a bug',
        href: 'https://github.com/w3c/performance-timeline/issues'
      }]
    }],
    wgPatentURI:  "http://www.w3.org/2004/01/pp-impl/45211/status"
  };
  </script>
</head>
<body>
  <section id='abstract'>
    <p>This specification extends the High Resolution Time specification
    [[!HR-TIME-2]] by providing methods to store and retrieve high resolution
    performance metric data.</p>
  </section>
  <section id='sotd'>
    <p>This new version is aligned with [[HR-TIME-2]] and introduces <a href=
    '#dictionary-PerformanceEntryFilterOptions-members'>filtering</a> and
    <a href="#the-performance-observer-interface">performance
    observers</a>.</p>
    <p>This is a <strong>work in progress</strong> and may change without any
    notices.</p>
  </section>
  <section class='informative'>
    <h2>Introduction</h2>
    <p>Accurately measuring performance characteristics of web applications is
    an important aspect of making web applications faster. This specification
    defines the necessary <a>Performance Timeline</a> primitives that enable
    web developers to access, instrument, and retrieve various performance
    metrics from the full lifecycle of a web application.</p>
    <p>[[NAVIGATION-TIMING-2]], [[RESOURCE-TIMING]], and [[USER-TIMING]] are
    examples of specifications that define timing information related to the
    navigation of the document, resources on the page, and developer scripts,
    respectively. Together these and other performance interfaces define
    performance metrics that describe the <a>Performance Timeline</a> of a web
    application. For example, the following script shows how a developer can
    access the <a>Performance Timeline</a> to obtain performance metrics
    related to the navigation of the document, resources on the page, and
    developer scripts:</p>
    <pre class="highlight example">
&lt;!doctype html&gt;
&lt;html&gt;
  &lt;head&gt;
  &lt;/head&gt;
  &lt;body onload="init()"&gt;
    &lt;img id="image0" src="http://www.w3.org/Icons/w3c_main.png" /&gt;
    &lt;script&gt;
       function init()
       {
            performance.mark("startWork"); // see [[User Timing]]
            doWork(); // Some developer code
            performance.mark("endWork");
            measurePerf();
       }
       function measurePerf()
       {
           var perfEntries = performance.getEntries();
           for (var i = 0; i &lt; perfEntries.length; i++)
           {
                 if (window.console) console.log("Name: "        + perfEntries[i].name      +
                                                 " Entry Type: " + perfEntries[i].entryType +
                                                 " Start Time: " + perfEntries[i].startTime +
                                                 " Duration: "   + perfEntries[i].duration  + "\n");
           }
       }
    &lt;/script&gt;
  &lt;/body&gt;
&lt;/html&gt;
</pre>
    <p>Alternatively, instead of processing metrics at a predefined time, or
    having to periodically poll the timeline for new metrics, the developer may
    also observe the <a>Performance Timeline</a> and be notified of new
    performance metrics via a <a>Performance Observer</a>:</p>
    <pre class="highlight example">
&lt;!doctype html&gt;
&lt;html&gt;
  &lt;head&gt;
  &lt;/head&gt;
  &lt;body&gt;
    &lt;img id="image0" src="http://www.w3.org/Icons/w3c_main.png" /&gt;
    &lt;script&gt;
    var observer = new PerformanceObserver(function(list) {
      var doneObservingEvents = false;
      var perfEntries = list.getEntries();
      for (var i = 0; i &lt; perfEntries.length; i++)
      {
           if (window.console) console.log("Name: "        + perfEntries[i].name      +
                                           " Entry Type: " + perfEntries[i].entryType +
                                           " Start Time: " + perfEntries[i].startTime +
                                           " Duration: "   + perfEntries[i].duration  + "\n");
      }
      // maybe disconnect after processing the events.
      if (doneObservingEvents) {
           observer.disconnect();
      }
    });
    // subscribe to Frame-Timing and User-Timing events
    observer.observe({entryTypes: ['render', 'composite', 'mark', 'measure']});
    &lt;/script&gt;
  &lt;/body&gt;
&lt;/html&gt;
</pre>
  </section>
  <section>
    <h2><dfn>Performance Timeline</dfn></h2>
    <p>Each <a href=
    "https://html.spec.whatwg.org/multipage/browsers.html#unit-of-related-similar-origin-browsing-contexts">
    unit of related similar-origin browsing contexts</a> has a <dfn>performance
    observer task queued flag</dfn> and an associated list of <a>registered
    performance observer</a> objects which is initially empty.</p>
    <p>To <dfn>queue a PerformanceEntry</dfn> (<i>new entry</i>), run these
    steps:</p>
    <ol>
      <li>Let <i>interested observers</i> be an initially empty set of
      <a>PerformanceObserver</a> objects.
      </li>
      <li>For each <a>registered performance observer</a> (<i>observer</i>):
        <ol>
          <li>If <i>observer</i>'s <a>PerformanceObserverInit</a> <a href=
          "#widl-PerformanceObserverInit-entryTypes">entryTypes</a> includes
          <i>new entry</i>’s <a href=
          "#widl-PerformanceEntry-entryType">entryType</a> value, append
          <i>observer</i> to <i>interested observers</i>.
          </li>
        </ol>
      </li>
      <li>For each <i>observer</i> in <i>interested observers</i>:
        <ol>
          <li>Append <i>new entry</i> to <a>observer buffer</a>.
          </li>
        </ol>
      </li>
      <li>If the <a>performance observer task queued flag</a> is set, terminate
      these steps.
      </li>
      <li>Set <a>performance observer task queued flag</a>.
      </li>
      <li>
        <a href=
        "https://html.spec.whatwg.org/multipage/webappapis.html#queue-a-task">Queue
        a task</a> that consists of running the following substeps. The
        <a href="https://html.spec.whatwg.org/multipage/webappapis.html#task-source">
        task source</a> for the queued task is the <i>performance timeline</i>
        task source.
        <ol>
          <li>Unset <a>performance observer task queued flag</a>.
          </li>
          <li>Let <i>notify list</i> be a copy of <a href=
          "https://html.spec.whatwg.org/multipage/browsers.html#unit-of-related-similar-origin-browsing-contexts">
            unit of related similar-origin browsing contexts’</a> list of
            <a>registered performance observer</a> objects.
          </li>
          <li>For each <a>PerformanceObserver</a> object <i>po</i> in <i>notify
          list</i>, run these steps:
            <ol>
              <li>Let <i>entries</i> be a copy of <i>po</i>’s <a>observer
              buffer</a>.
              </li>
              <li>Empty <i>po</i>’s <a>observer buffer</a>.
              </li>
              <li>If <i>entries</i> is non-empty, call <i>po</i>’s callback
              with <i>entries</i> as first argument and <a href=
              "https://heycam.github.io/webidl/#dfn-callback-this-value">callback
              this value</a>. If this throws an exception, <a href=
              "https://html.spec.whatwg.org/multipage/webappapis.html#report-the-exception">
                report the exception</a>.
              </li>
            </ol>
          </li>
        </ol>
      </li>
    </ol>
    <p>The <i>performance timeline</i> <a href=
    "https://html.spec.whatwg.org/multipage/webappapis.html#task-queue">task
    queue</a> is a low priority queue that, if possible, should be processed by
    the user agent during idle periods to minimize impact of performance
    monitoring code.</p>
    <section>
      <h2>The <dfn>PerformanceEntry</dfn> interface</h2>
      <p>The <a>PerformanceEntry</a> interface hosts the performance data of
      various metrics.</p>
      <dl title='[Exposed=(Window,Worker)] interface PerformanceEntry' class=
      'idl'>
        <dt>readonly attribute DOMString name</dt>
        <dd>
          The attribute MUST return a <a href=
          "http://www.w3.org/TR/WebIDL/#idl-DOMString">DOMString</a> identifier
          for this <a>PerformanceEntry</a> object. This identifier does not
          have to be unique.
        </dd>
        <dt>readonly attribute DOMString entryType</dt>
        <dd>
          This attribute MUST return the <a href=
          "http://www.w3.org/TR/WebIDL/#idl-DOMString">DOMString</a> that
          describes the type of the interface represented by this
          <a>PerformanceEntry</a> object.
          <p class="note">Valid entryType values are: composite
          [[!FRAME-TIMING]], mark [[!USER-TIMING]], measure [[!USER-TIMING]],
          navigation [[!NAVIGATION-TIMING]], render [[!FRAME-TIMING]], resource
          [[!RESOURCE-TIMING]], server [[!SERVER-TIMING]].</p>
        </dd>
        <dt>readonly attribute DOMHighResTimeStamp startTime</dt>
        <dd>
          The attribute MUST return a <a href=
          "https://w3c.github.io/hr-time/#dom-domhighrestimestamp">DOMHighResTimeStamp</a>
          that contains the time value of the first recorded timestamp of this
          performance metric.
        </dd>
        <dt>readonly attribute DOMHighResTimeStamp duration</dt>
        <dd>
          The attribute MUST return a <a href=
          "https://w3c.github.io/hr-time/#dom-domhighrestimestamp">DOMHighResTimeStamp</a>
          that contains the time value of the duration of the entire event
          being recorded by this <a>PerformanceEntry</a>. Typically, this would
          be the time difference between the last recorded timestamp and the
          first recorded timestamp of this <a>PerformanceEntry</a>. If the
          duration concept doesn't apply, a performance metric may choose to
          return a `duration` of 0.
        </dd>
        <dt>serializer = { attribute }</dt>
      </dl>
    </section>
    <section>
      <h2>Extensions to the <dfn>Performance</dfn> interface</h2>
      <p>This extends the <a href=
      'https://w3c.github.io/hr-time/#idl-def-Performance'>Performance</a>
      interface [[!HR-TIME-2]] and hosts performance related attributes and
      methods used to retrieve the performance metric data from the
      <a>Performance Timeline</a>.</p>
      <dl title='dictionary PerformanceEntryFilterOptions' class='idl'>
        <dt>DOMString name</dt>
        <dd>
          <a href="#widl-PerformanceEntry-name">name</a> of `PerformanceEntry`
          object.
        </dd>
        <dt>DOMString entryType</dt>
        <dd>
          <a href="#widl-PerformanceEntry-entryType">entryType</a> of
          `PerformanceEntry` object.
        </dd>
        <dt>DOMString initiatorType</dt>
        <dd>
          <a href=
          "http://www.w3.org/TR/resource-timing/#widl-PerformanceResourceTiming-initiatorType">
          initiatorType</a> of `PerformanceEntry` object.
        </dd>
      </dl>
      <dl title='partial interface Performance' class='idl'>
        <dt>PerformanceEntryList getEntries(optional
        PerformanceEntryFilterOptions filter)</dt>
        <dd>
          <p>This method returns a <a>PerformanceEntryList</a> object that
          contains a list of <a>PerformanceEntry</a> objects, sorted in
          chronological order with respect to <a href=
          "#widl-PerformanceEntry-startTime">startTime</a>, that match the
          following criteria:</p>
          <ol>
            <li>Let the <dfn>list of entry objects</dfn> be the empty
            <a>PerformanceEntryList</a>.
            </li>
            <li>Let the <dfn>set of filter properties</dfn> be a set of pairs
            where the first element is the _name_ of a <a href=
            "http://www.w3.org/TR/WebIDL/#dfn-dictionary-member">dictionary
            member</a> of `filter` that is present and the second element is
            the _value_ of that <a href=
            "http://www.w3.org/TR/WebIDL/#dfn-dictionary-member">dictionary
            member</a>.
            </li>
            <li>For each <a>PerformanceEntry</a> object
            (<dfn>entryObject</dfn>) associated with the <a href=
            "http://www.w3.org/TR/dom/#context-object">context object</a>, in
            chronological order with respect to <a href=
            "#widl-PerformanceEntry-startTime">startTime</a>:
              <ol>
                <li>For each _name_ and _value_ pair in <a>set of filter
                properties</a>:
                  <ol>
                    <li>If the <a>entryObject</a> does not contain an attribute
                    whose name matches _name_, go to next <a>entryObject</a>.
                    </li>
                    <li>Otherwise, if the <a>entryObject</a> contains an
                    attribute whose name matches _name_, and its value does not
                    match _value_, go to next <a>entryObject</a>.
                    </li>
                  </ol>
                </li>
                <li>Add <a>entryObject</a> to the <a>list of entry objects</a>.
                </li>
              </ol>
            </li>
            <li>Return the <a>list of entry objects</a>.
            </li>
          </ol>
        </dd>
        <dt>PerformanceEntryList getEntriesByType(DOMString type)</dt>
        <dd>
          This method returns a <a>PerformanceEntryList</a> object returned by
          `getEntries({'entryType': type})`.
        </dd>
        <dt>PerformanceEntryList getEntriesByName(DOMString name, optional
        DOMString type)</dt>
        <dd>
          This method returns a <a>PerformanceEntryList</a> object returned by
          `getEntries({'name': name})` if optional `entryType` is omitted, and
          `getEntries({'name': name, 'entryType': type})` otherwise.
        </dd>
      </dl>
      <dl class='idl' title=
      'typedef sequence&lt;PerformanceEntry&gt; PerformanceEntryList'></dl>
    </section>
    <section>
      <h2>The <dfn>Performance Observer</dfn> interface</h2>
      <p>The <a>PerformanceObserver</a> interface can be used to observe the
      <a>Performance Timeline</a> and be notified of new performance entries as
      they are recorded by the user agent.</p>
      <ul>
        <li>A <dfn>registered performance observer</dfn> consists of an
        observer (a <a>PerformanceObserver</a> object) and options (a
        <a>PerformanceObserverInit</a> dictionary).
        </li>
        <li>Each <a>registered performance observer</a> object has a private
        list of <a>PerformanceEntry</a> objects (<dfn>observer buffer</dfn>)
        which is initially empty.
        </li>
      </ul>
      <dl title=
      'callback PerformanceObserverCallback = void (PerformanceObserverEntryList entries, PerformanceObserver observer)'
      class='idl'></dl>
      <dl title=
      '[Constructor(PerformanceObserverCallback callback), Exposed=(Window,Worker)] interface PerformanceObserver'
      class='idl'>
        <dt>void observe(PerformanceObserverInit options)</dt>
        <dd>
          This method instructs the user agent to <dfn>register the
          observer</dfn> and report any new performance entries based on the
          criteria given by <a>options</a>.
          <dl title='dictionary PerformanceObserverInit' class='idl'>
            <dt>required sequence&lt;DOMString&gt; entryTypes</dt>
            <dd>
              <p>A list of valid <a href=
              "#widl-PerformanceEntry-entryType">entryType names</a> to be
              observed. The list MUST NOT be empty and types not recognized by
              the user agent MUST be ignored.</p>
              <p class="note">To keep the performance overhead to minimum the
              application should only subscribe to event types that it is
              interested in, and disconnect the observer once it no longer
              needs to observe the performance data. Filtering by name is not
              supported, as it would implicitly require a subscription for all
              event types — this is possible, but discouraged, as it will
              generate a significant volume of events.</p>
            </dd>
          </dl>
          <p>The `observe(options)` method must run these steps:</p>
          <ol>
            <li>If _options'_ `entryTypes` attribute is not present, <a href=
            "http://heycam.github.io/webidl/#dfn-throw">throw</a> a JavaScript
            `TypeError`.
            </li>
            <li>Filter unsupported <a href="#widl-PerformanceEntry-entryType">
              entryType names</a> within the `entryTypes` sequence, and replace
              the `entryTypes` sequence with the new filtered sequence.
            </li>
            <li>If the _options'_ `entryTypes` attribute is an empty sequence,
            <a href="http://heycam.github.io/webidl/#dfn-throw">throw</a> a
            JavaScript `TypeError`.
            </li>
            <li>If the <a>performance observer</a> is already registered with a
            <a>Performance Timeline</a>, replace the <a>performance
            observer</a> `options` with _options_.
            </li>
            <li>Otherwise, register the <a>performance observer</a> as an <dfn>
              observer</dfn> with the `options` as _options_ on the
              <a>Performance Timeline</a> visible from the <a>performance
              observer</a>'s context.
            </li>
          </ol>
        </dd>
        <dt>void disconnect()</dt>
        <dd>
          This method must remove the <a>registered performance observer</a>
          from the <a>Performance Timeline</a> for which it is an
          <a>observer</a>.
        </dd>
      </dl>
      <section>
        <h2>The <dfn>PerformanceObserverEntryList</dfn> interface</h2>
        <p>The <a>PerformanceObserverEntryList</a> interface provides the same
        `getEntries`, `getEntriesByType`, `getEntriesByName` methods as the
        <a>Performance</a> interface, except that
        <a>PerformanceObserverEntryList</a> operates on the observed emitted
        list of events instead of the global timeline.</p>
        <dl title=
        '[Exposed=(Window,Worker)] interface PerformanceObserverEntryList'
        class='idl'>
          <dt>PerformanceEntryList getEntries(optional
          PerformanceEntryFilterOptions filter)</dt>
          <dd>
            See <a href=
            "#widl-PerformanceObserverEntryList-getEntries-PerformanceEntryList-PerformanceEntryFilterOptions-filter">
            performance.getEntries</a>.
          </dd>
          <dt>PerformanceEntryList getEntriesByType(DOMString type)</dt>
          <dd>
            See <a href=
            "#widl-PerformanceObserverEntryList-getEntriesByType-PerformanceEntryList-DOMString-entryType">
            performance.getEntriesByType</a>.
          </dd>
          <dt>PerformanceEntryList getEntriesByName(DOMString name, optional
          DOMString type)</dt>
          <dd>
            See <a href=
            "#widl-PerformanceObserverEntryList-getEntriesByName-PerformanceEntryList-DOMString-name-DOMString-entryType">
            performance.getEntriesByName</a>.
          </dd>
        </dl>
      </section>
    </section>
  </section>
</body>
</html>
